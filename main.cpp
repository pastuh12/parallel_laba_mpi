
#include <iostream>
#include <iomanip>
#include "mpi.h"
#include <ctime> 

using namespace std;

double f(double x) {
    return 1 / (1 + x * x);
}

int main(int argc, char* argv[])
{
    setlocale(LC_ALL, "rus");
    int size, rank, n;
    long double h = 0, step, s = 0, x, y, sum1 = 0, sum2 = 0, sum = 0, a = 0, b = 5;
    double startwtime = 0.0;
    double endwtime;


    cout << "Введите n - разбиение промежутка интегрирования: ";
    cin >> n;

    cout << endl << "Введите начало промежутка интегрирования: ";
    cin >> a;
    cout << endl << "Введите конец промежутка интегрирования: ";
    cin >> b;
    //непараллельная 
    step = (b - a) / (double)n;

    //startwtime = MPI_Wtime();
    for (int i = 0; i < n; i++)
    {
        x = a + i * step;
        y = x * x;
        s = s + y * step;
    }
    cout << "Значение интеграла: " << s;
    cout << "Время выполнения непараллельной программы: " << endl;
    s = 0;
    //startwtime = MPI_Wtime();
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Status status;
    if (rank == 0) {
        startwtime = MPI_Wtime();
        /*cout << "Enter the number of splits: "; cin >> n;
        cout << "Enter the beginning and end of the segment: "; cin >> a >> b;*/
        for (int i = 1; i < size; i++) {
            MPI_Send(&n, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
            MPI_Send(&a, 1, MPI_DOUBLE, i, 1, MPI_COMM_WORLD);
            MPI_Send(&b, 1, MPI_DOUBLE, i, 2, MPI_COMM_WORLD);
        }
    }
    else {
        MPI_Recv(&n, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
        MPI_Recv(&a, 1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, &status);
        MPI_Recv(&b, 1, MPI_DOUBLE, 0, 2, MPI_COMM_WORLD, &status);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    for (int i = 1; i < n; i++ )
    {
        //MPI_Barrier(MPI_COMM_WORLD);
        x = a + i * step;
        y = x * x;
        s = s + y * step;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    if (rank == 0) {
        cout << "Значение интеграла: " << s;
        cout << "Время выполнения непараллельной программы: " << (MPI_Wtime() - startwtime) << endl;
    }
    MPI_Finalize();
    return 0;
}
